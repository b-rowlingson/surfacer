lancaster <- function(){
    xlancaster = data.frame(lat=54.04649, lon=-2.79988)
    coordinates(xlancaster)=~lon+lat
    proj4string(xlancaster)=CRS("+init=epsg:4326")
    xlancaster = spTransform(xlancaster, CRS("+init=epsg:27700"))
    xlancaster
}

makedem <- function(loc=lancaster(), nrows=100, ncols=100){


    pts = coordinates(loc)
    pts = data.frame(rbind(pts, pts+1000))
    
    coordinates(pts)=~lon+lat
    proj4string(pts)=CRS("+init=epsg:27700")

    r = raster(extent(pts), nrows=nrows, ncols=ncols)
    projection(r)=CRS("+init=epsg:27700")
    r
    
}

pathpts = function(){
    spp = cbind(
        c(347900, 348500),
        c(461700,  462000)
    )
    list(start=spp[1,],
         end=spp[2,])
}

mount <- function(ras,scale=50){
    outer(1:nrow(ras), 1:ncol(ras),
          function(x,y){
              r = sqrt((x-(nrow(ras)/2))^2 + (y-(ncol(ras)/2 ))^2)
              exp(r/scale)
          }
          )
}


dtran <- function(r,nd=16){
### this is not right...
    ds2 = res(r)[1]^2
    distH <- function(x){1/((sqrt(ds2+(x[2] - x[1])^2)))}
    transition(r, distH, nd, symm=TRUE)
}


distadj <- function(r){
    ## skeleton transition matrix
    tm = transition(r, function(x){0}, 16)

    ## get the pairs of adjacencies
    adj = adjacent(r, 1:ncell(r), directions=16, pairs=TRUE)

    ## construct x,y,h of from and to cells:
    M = data.frame(from = xyFromCell(r, adj[,1]), from.z=r[adj[,1]], to=xyFromCell(r, adj[,2]), to.z=r[adj[,2]])

    ## compute 3d distance, assign over values in skeleton transition matrix:
    d = sqrt((M$from.x-M$to.x)^2 + (M$from.y-M$to.y)^2 + (M$from.z-M$to.z)^2)

    ## use 1/d for conductance
    tm[adj]=1/d
    tm
}

demo_path <- function(){
    r = makedem(ncol=100,nrow=100)
    r[] = mount(r, scale=25)
    par(mfrow=c(2,2))
    for(scale in c(50,100,500,1000)){
        rscale = r*scale
        plot(rscale)
        tm = distadj(rscale)
        plot(shortestPath(tm, pathpts()$start, pathpts()$end,"SpatialLines"),add=TRUE)
    }
}
    
